// identify which components are needd to build the navigation
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap'

// implement links in the NavBar
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';


// we will now describe how we want our NavBar to look
function AppNavBar() {
	// lets destructure the context object and consume the needed information to properly render the navbar
	const { user } = useContext(UserContext);
	return(
		<Navbar className="fixed-top" bg="success" expand="lg">
			<Container>
				<Navbar.Brand> B156 Booking App </Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse>
						<Nav className="ml-auto">
							<Link className="nav-link navi" to="/">
								Home
							</Link>
							{user.id !== null ?
								<Link className="nav-link navi" to="/logout">
									Logout
								</Link>
							:
								<>
									<Link className="nav-link navi" to="/register">
										Register
									</Link>
									<Link className="nav-link navi" to="/login">
										Login
									</Link>
								</>
							}

							<Link className="nav-link navi" to="/courses">
								Courses
							</Link>
							<Link className="nav-link navi" to="/add">
								Add Courses
							</Link>
							<Link className="nav-link navi" to="/update">
								Update Courses
							</Link>
						</Nav>
					</Navbar.Collapse>
			</Container>
		</Navbar>		
	);
};

export default AppNavBar;