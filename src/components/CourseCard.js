// identify needed components
import {Link} from 'react-router-dom';
import { Row, Col, Card } from 'react-bootstrap';

/*<Card className="course-card m-3 d-md-inline-flex d-sm-inline-flex text-center">
	<Card.Body>
		<Card.Title>
			{courseProp.name}
		</Card.Title>
		<Card.Text>
			{courseProp.description}
		</Card.Text>
		<Card.Text>
			&#8369; {courseProp.price}
		</Card.Text>
		<Link to={`view/${courseProp._id}`} className="btn btn-success">
			View Course
		</Link>
	</Card.Body>
</Card>	*/

export default function CourseCard({courseProp}) {
	return(
		<Row className="m-3 d-md-inline-flex d-sm-inline-flex">
			<Col xs={12} md={4}>
				<Card className="course-card text-center">
					<Card.Body>
						<Card.Title>
							{courseProp.name}
						</Card.Title>
						<Card.Text>
							{courseProp.description}
						</Card.Text>
						<Card.Text>
							&#8369; {courseProp.price}
						</Card.Text>
						<Link to={`view/${courseProp._id}`} className="btn btn-success">
							View Course
						</Link>
					</Card.Body>
				</Card>					
			</Col>
		</Row>

	)
}