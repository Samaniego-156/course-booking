import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
// acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import Catalog from './pages/Courses';
import Login from './pages/Login';
import CourseView from './pages/CourseView';
import AddCourse from './pages/AddCourse';
import Update from './pages/UpdateCourse';
import Error from './pages/Error';
import Logout from './pages/Logout';

import './App.css'

// acquire the provider utility in this entry point
import { UserProvider } from './UserContext';

// implement page routing in our app
// acquire the utilities from the react-router-dom
import {BrowserRouter as Router, Routes, Route}from 'react-router-dom';

// BrowserRouter -> this is a standard library component for routing in react. This
// enables navigation amongst views of various components. It will serve as the parent component
// that will be used to store  all the other components.
// ** as -> alias keyword in JS

// Routes -> it's a new component introduced in version 6 of react-router whose task is to
// allow switching between locations.

// JSX Components -> self closing
// syntax: <Element />
function App() {

  // the role of the provider was assigned to the App.js,which means all the information that we will
  // declare here will automatically become 'global' scope.
  // initialize state of the user to provide and identify the status of the client who is using the app.
  
  // id => to properly reference the user
  // isAdmin => to identify the role of the user
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    }); //binds the user to the default state

  // feed the information stated here to the consumers

  // Create a function that will unset/unmount the user
  const unsetUser = () => {
    // clear out the saved data in the local storage
    localStorage.clear();
    // revert the state of the user back to null
    setUser({
      id: null,
      isAdmin: null
    });
  }

  // Create a 'side-effect ' 
  useEffect(() => {
    // console.log(user);
    // mount or campaign the user to the app so that he/she will be recognized, using the token

    // retrieve the token from the browser storage
    let token = localStorage.getItem('access');
    // console.log(token);

    // fetch('URL', {options}), keep in mind in this request we are passing down a token.
    fetch('https://stark-reef-18545.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      // console.log(convertedData);
      // identify the procedures taht will happen if the info  about the user is retrieved successfully
      // change the current state of the user
      if (typeof convertedData._id !== "undefined") {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        });
      } else {
        // if the condition above is not met
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });

  },[user]);

  return (
        <div>
          <UserProvider value={ {user, setUser, unsetUser} } >
            <Router>
              <AppNavBar />
              <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/register' element={<Register />} />
                <Route path='/courses' element={<Catalog />} />
                <Route path='/login' element={<Login />}/>
                <Route path='/courses/view/:id' element={<CourseView />} />
                <Route path='/add' element={<AddCourse />} />
                <Route path='/update' element={<Update />} />
                <Route path='/logout' element={<Logout />} />
                <Route path='*' element={<Error />} />
              </Routes>
            </Router>            
          </UserProvider>
        </div>
  );
}

export default App;
