// identify the components that will be used for this page

import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

import Bruce from './../components/Banner';
import {Form, Button, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

// we will declare states for out form components for us to be able to access and manage the values in each of the form elements

const data = {
	title: "Welcome to Login Page",
	content: "Sign in your account below"
}

// Create a function that will describe the structure of the page
export default function Login(){

	const {user, setUser} = useContext(UserContext);

	// Declare an 'initial'/default state for our form elements
	// Bind the form elements to the desired states
	// Assign the states to their respective components
	// SYNTAX: const/let [getter, setter] = useState()

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// what validation are we going to run on the email. 
	// (format: @ , dns)
	let addressSign = email.search('@');
	// if the search() finds no match inside the string it will return -1.
	let dns = email.search('.com');

	// declare a state for the login button
	const [isActive, setIsActive] = useState(false);
	// apply a conditional rendering to the button component for its current state
	const [isValid, setIsValid] = useState(false);

	// create a side effect that will make our page 'reactive'
	useEffect(() => {
		// Create a logic /condition that will evaluate the format of the email.
		// if the value is -1 (no match found inside the string)
		if (dns !== -1 && addressSign !== -1) {
			setIsValid(true);
			if (password!== '') {
				setIsActive(true);
			} else {
				setIsActive(false);
			}
		} else {
			setIsValid(false);
			setIsActive(false);
		}
	},[email, password, dns, addressSign]);

	// Create a function that will run the request for user authentication to produce an access token

	const loginUser = async (event) => {
		event.preventDefault()

			// send a request to verify if the user's identity is true
			// Syntax: fetch(URL,{Options})
			fetch('https://stark-reef-18545.herokuapp.com/users/login', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					    "email": email,
					    "password": password
				})
			}).then(res => res.json())
			.then(data => {
				let token = data.access;
				console.log(token);

				// create a control structure to give a proper response to the user
				if (typeof token !== 'undefined') {
					// prompt message to user

					// The produced token, save it on the browser storage. (storage area)
					// to save an item in the localStorage object of the browser.. use setItem(key, value)
					localStorage.setItem('access', token);

					fetch('https://stark-reef-18545.herokuapp.com/users/details', {
					  headers: {
					    Authorization: `Bearer ${token}`
					  }
					})
					.then(res => res.json())
					.then(convertedData => {
					  // identify the procedures taht will happen if the info  about the user is retrieved successfully
					  // change the current state of the user
					  if (typeof convertedData._id !== "undefined") {
					    setUser({
					      id: convertedData._id,
					      isAdmin: convertedData.isAdmin
					    });
						Swal.fire({
							icon: 'success',
							title: 'Login Successful',
							text: 'You have successfully logged in.'
						});

					  } else {
					    // if the condition above is not met
					    setUser({
					      id: null,
					      isAdmin: null
					    });
					  }
					});

				} else {
					Swal.fire({
						icon: 'error',
						title: 'Check Your Credentials',
						text: 'Contact Admin If Problem Persists'
					})						
				}
			})


	};

	return(
		user.id
		?
			<Navigate to="/courses" replace={true} />
		:
		<>
			<Bruce bannerData={data} />

			<Container className="container__box">
				<h1 className="mt-2 text-center"> Login Form </h1>
				<Form onSubmit={e => loginUser(e)}>
					{/*Email Address Field*/}
					<Form.Group>
						<Form.Label>Email: </Form.Label>
						<Form.Control type="email" placeholder="Enter email here" required value={email} onChange={e => {setEmail(e.target.value)} } />

						{
							isValid ?
								<h6 className="text-success"> Email is Valid </h6>
							:
								<h6 className="text-mute"> Email is Invalid </h6>					
						}

					</Form.Group>

					{/*Password Field*/}
					<Form.Group>
						<Form.Label>Password: </Form.Label>
						<Form.Control type="password" placeholder="Enter password here" required value={password} onChange={e => {setPassword(e.target.value)}}  />
					</Form.Group>

					{
						isActive ?
							<Button variant="success" type="submit" className="mb-3 btn-block"> Login </Button>
						:
							<Button variant="secondary" type="submit" className="mb-3 btn-block" disabled> Login </Button>
					}
				</Form>
			</Container>
		</>
	);
};