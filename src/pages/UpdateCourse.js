import Hero from '../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Welcome to the Update Course Page',
	content: 'You can update details for a course here'
}

export default function Update() {
	// catch the 'click' event that will happen on the button component
	const updateCourse = (eventSubmit) => {
		eventSubmit.preventDefault()
		return(
			// display a message that will confirm to the user that registration is successful.
			Swal.fire({
				icon: 'success',
				title: 'Update Successful',
				text: 'Successfully updated the details of the course.' 
			})
		);
	};

	return(
		<>
			<Hero bannerData={data} />
			<Container className="container__box">
				<h1 className="text-center mt-3">Update Course Form</h1>
				<Form onSubmit={e => updateCourse(e)}>
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your course name" required />
					</Form.Group>
					{/*Description Field*/}
					<Form.Group>
						<Form.Label>Description: </Form.Label>
						<Form.Control type="text" placeholder="Enter a description here" required />
					</Form.Group>
					{/*Price*/}
					<Form.Group>
						<Form.Label>Price: </Form.Label>
						<Form.Control type="number" placeholder="Enter your price" required />
					</Form.Group>
					{/*Enable/Disable Course*/}
					<Form.Group>
						<Form.Check 
						  type="switch"
						  id="custom-switch"
						  label="Enable Course"
						/>
					</Form.Group>
					{/*Register Button*/}
					<Button className="mb-3 btn-success btn-block" type="submit"> Update </Button>
				</Form>
			</Container>
		</>
	);
};