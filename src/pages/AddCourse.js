import Bruce from '../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
	title: 'Welcome to the Add Course Page',
	content: 'You can create a new course/subject to add to our list courses available.'
}

export default function NewCourse() {
	const addCourse = (event) => {
		event.preventDefault()
		return(
			Swal.fire({
				icon: 'success',
				title: 'Course Added',
				text: 'New course was successfully added!' 
			})
		);
	};

	return(
		<>
			<Bruce bannerData={data} />
			<Container className="container__box">
				<h1 className="text-center">Create Course Form</h1>
				<Form onSubmit={e => addCourse(e)}>
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name: </Form.Label>
						<Form.Control type="text" placeholder="Enter your course name" required />
					</Form.Group>
					{/*Description Field*/}
					<Form.Group>
						<Form.Label>Description: </Form.Label>
						<Form.Control type="text" placeholder="Enter description" required />
					</Form.Group>
					{/*Price Field*/}
					<Form.Group>
						<Form.Label>Price: </Form.Label>
						<Form.Control type="number" placeholder="Enter price" required />
					</Form.Group>
					<Button className="mb-3 btn-success btn-block" type="submit"> Add Course </Button>
				</Form>
			</Container>
		</>
	);
};